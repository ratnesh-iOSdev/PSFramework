//
//  PSFramework.h
//  PSFramework
//
//  Created by Ratnesh Shukla on 01/07/15.
//  Copyright (c) 2015 Ratnesh Shukla. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PSACL.h"
#import "PSAnalytics.h"
#import "PSAnonymousUtils.h"
#import "PSCloud.h"
#import "PSConfig.h"
#import "PSConstant.h"
#import "PSFile.h"
#import "PSGeoPoint.h"
//#import "PSObject+Subclass.h"
#import "PSObject.h"
#import "PSQuery.h"
#import "PSRelation.h"
#import "PSRole.h"
#import "PSSession.h"
#import "PSSubclassing.h"
#import "PSUser.h"
#import "PSInstallation.h"
#import "PSNetworkActivityIndicatorManager.h"
#import "PSNullability.h"
#import "PSProduct.h"
#import "PSPurchase.h"
#import "PSPush.h"
#import "PSTwitterUtils.h"


@interface PSFramework : NSObject
///--------------------------------------
/// @name Connecting to TestServer
///--------------------------------------

/*!
 @abstract Sets the applicationId and clientKey of your application.
 
 @param applicationId The application id of your Parse application.
 @param clientKey The client key of your Parse application.
 */
+ (void)setApplicationId:(NSString *)applicationId clientKey:(NSString *)clientKey;

/*!
 @abstract The current application id that was used to configure Parse framework.
 */
+ (NSString *)getApplicationId;

/*!
 @abstract The current client key that was used to configure Parse framework.
 */
+ (NSString *)getClientKey;

///--------------------------------------
/// @name Enabling Local Datastore
///--------------------------------------

/*!
 @abstract Enable pinning in your application. This must be called before your application can use
 pinning. The recommended way is to call this method before `setApplicationId:clientKey:`.
 */
+ (void)enableLocalDatastore;

/*!
 @abstract Flag that indicates whether Local Datastore is enabled.
 
 @returns `YES` if Local Datastore is enabled, otherwise `NO`.
 */
+ (BOOL)isLocalDatastoreEnabled;

///--------------------------------------
/// @name Enabling Extensions Data Sharing
///--------------------------------------
+ (void)enableDataSharingWithApplicationGroupIdentifier:(NSString *)groupIdentifier
                                  containingApplication:(NSString *)bundleIdentifier;

/*!
 @abstract Application Group Identifier for Data Sharing
 
 @returns `NSString` value if data sharing is enabled, otherwise `nil`.
 */
+ (NSString *)applicationGroupIdentifierForDataSharing;

/*!
 @abstract Containing application bundle identifier.
 
 @returns `NSString` value if data sharing is enabled, otherwise `nil`.
 */
+ (NSString *)containingApplicationBundleIdentifierForDataSharing;


///--------------------------------------
/// @name Logging
///--------------------------------------

/*!
 @abstract Sets the level of logging to display.
 
 @discussion By default:
 - If running inside an app that was downloaded from iOS App Store - it is set to <PSLogLevelNone>
 - All other cases - it is set to <PSLogLevelWarning>
 
 @param logLevel Log level to set.
 @see PSLogLevel
 */
+ (void)setLogLevel:(PSLogLevel)logLevel;

/*!
 @abstract Log level that will be displayed.
 
 @discussion By default:
 - If running inside an app that was downloaded from iOS App Store - it is set to <PSLogLevelNone>
 - All other cases - it is set to <PSLogLevelWarning>
 
 @returns A <PSLogLevel> value.
 @see PSLogLevel
 */
+ (PSLogLevel)logLevel;


@end
